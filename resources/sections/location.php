<section id="contact" class="white_section">
	<h2><?= $lang['location.title'] ?></h2>

	<div>
		<div class="informations">
			<div class="information">
				<img src="../images/icon-man.svg" alt="Icon Man">
				<span>Trattoria San Pietro</span>
			</div>
			<div class="information">
				<img src="../images/icon-map.svg" alt="Icon Man">
				<span>Via S. Pietro, 95,<br>35139 Padova PD,<br>Italie</span>
			</div>
			<div class="information">
				<img src="../images/icon-mobile.svg" alt="Icon Man">
				<span><a href="tel:+390498760330" style="text-decoration: none; color: #009900;">+390498760330</a></span>
			</div>
		</div>

		<div class="location" style="background-image: url('../images/location.png')"></div>
	</div>
</section>
