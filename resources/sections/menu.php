<section id="carte" class="white_section">
	<h2><?= $lang['menu.title'] ?></h2>

	<div class="carte">
		<article class="menu">
			<h3><?= $lang['menu.entrees'] ?></h3>

				<div class="menu-image" style="background-image: url(<?= $menu['entrees']['image'] ?>)"></div>

				<div class="menu-recipes">
                    <?php foreach ($menu['entrees']['choices'] as $item): ?>
					<div class="menu-recipe">
						<div><?= $item['name'] ?></div>
						<div class="points-separator"></div>
						<div><?= $item['price'] ?>€</div>
					</div>
					<?php endforeach; ?>
				</div>
		</article>

		<article class="menu">
			<h3><?= $lang['menu.meals'] ?></h3>

			<div class="menu-image" style="background-image: url(<?= $menu['meals']['image'] ?>)"></div>

			<div class="menu-recipes">
                <?php foreach ($menu['meals']['choices'] as $item): ?>
					<div class="menu-recipe">
						<div><?= $item['name'] ?></div>
						<div class="points-separator"></div>
						<div><?= $item['price'] ?>€</div>
					</div>
                <?php endforeach; ?>
			</div>
		</article>

		<article class="menu">
			<h3><?= $lang['menu.deserts'] ?></h3>

			<div class="menu-image" style="background-image: url(<?= $menu['deserts']['image'] ?>)"></div>

			<div class="menu-recipes">
                <?php foreach ($menu['deserts']['choices'] as $item): ?>
					<div class="menu-recipe">
						<div><?= $item['name'] ?></div>
						<div class="points-separator"></div>
						<div><?= $item['price'] ?>€</div>
					</div>
                <?php endforeach; ?>
			</div>
		</article>
	</div>
</section>
