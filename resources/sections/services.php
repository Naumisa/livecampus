<section id="services" class="white_section">
	<h2><?= $lang['services.title'] ?></h2>

	<div class="menu-services">
		<?php foreach ($services as $service): ?>
		<div class="duo-services <?= $service['add_class'] ?>">
			<div class="service service-image" style="background-image: url(<?= $service['image'] ?>)"></div>

			<div class="service service-text">
				<p>
                    <?= $lang[$service['description']] ?>
				</p>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</section>
