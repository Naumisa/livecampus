<section id="contact" class="white_section">
	<h2>Bonjour, <?= $data['user_name'] ?></h2>

	<div id="contact_form">
		<ul>
			<li>
				<label for="name">Nom :</label>
				<input type="text" id="name" name="user_name" value="<?= $data['user_name'] ?>" disabled>
			</li>
			<li>
				<label for="mail">E-mail :</label>
				<input type="email" id="mail" name="user_email" value="<?= $data['user_email'] ?>" disabled>
			</li>
			<li>
				<label for="job">Profession :</label>
				<input type="text" id="job" name="user_job" value="<?= $data['user_job'] ?>" disabled>
			</li>
			<li>
				<label for="mobile">Mobile :</label>
				<input type="tel" id="mobile" name="user_mobile" value="<?= $data['user_mobile'] ?>" disabled>
			</li>
			<li>
				<label for="msg">Message :</label>
				<textarea id="msg" name="user_message" disabled><?= $data['user_message'] ?></textarea>
			</li>
		</ul>
	</div>
</section>
