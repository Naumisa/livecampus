<section id="contact" class="white_section">
	<h2><?= $lang['contact.title'] ?></h2>

	<div id="contact_form">
		<form method="post">
			<ul>
				<li>
					<label for="name">Nom :</label>
					<input type="text" id="name" name="user_name" value="<?= $data['user_name'] != null ? $data['user_name'] : '' ?>" required>
				</li>
				<li>
					<label for="mail">E-mail :</label>
					<input type="email" id="mail" name="user_email" value="<?= $data['user_email'] != null ? $data['user_email'] : '' ?>" required>
				</li>
				<li>
					<label for="job">Profession :</label>
					<input type="text" id="job" name="user_job" value="<?= $data['user_job'] != null ? $data['user_job'] : '' ?>" required>
				</li>
				<li>
					<label for="mobile">Mobile :</label>
					<input type="tel" id="mobile" name="user_mobile" value="<?= $data['user_mobile'] != null ? $data['user_mobile'] : '' ?>" required>
				</li>
				<li>
					<label for="msg">Message :</label>
					<textarea id="msg" name="user_message" required><?= $data['user_message'] != null ? $data['user_message'] : '' ?></textarea>
				</li>
				<?php if ($error !== ''): ?>
				<li>
					<textarea id="error" disabled><?= $error ?></textarea>
				</li>
				<?php endif; ?>
				<li>
					<input type="submit" value="Envoyer">
				</li>
			</ul>
		</form>
	</div>
</section>
