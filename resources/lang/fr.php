<?php
	return [
		'web_title' => 'Trattoria',

		'title' => 'San Pietro',
        'contact_us' => 'NOUS CONTACTER',
        'home' => 'Accueil',
        'menu' => 'Menu',
        'services' => 'Services',
        'contact' => 'Contact',
        'location' => 'Localisation',

        'menu.title' => 'Notre carte',
        'menu.entrees' => 'Entrées',
        'menu.meals' => 'Plats',
        'menu.deserts' => 'Desserts',

        'services.title' => 'Nos services',
        'services.service_01' => 'Toutes nos préparations sont réalisés sur place.',
        'services.service_02' => 'Nos aliments sont frais, locaux et sélectionnés avec soins.',
        'services.service_03' => 'Accueil et service agréable et de qualité.',

        'contact.title' => 'Nous contacter',

        'location.title' => 'Nous trouver',
	];
