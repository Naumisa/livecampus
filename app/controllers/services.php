<?php
    $services = [
        '0' => [
            'image' => '../images/service-01.png',
            'description' => 'services.service_01',
            'add_class' => '',
        ],
        '1' => [
            'image' => '../images/service-02.png',
            'description' => 'services.service_02',
            'add_class' => 'reverse',
        ],
        '2' => [
            'image' => '../images/service-03.png',
            'description' => 'services.service_03',
            'add_class' => '',
        ],
    ];
