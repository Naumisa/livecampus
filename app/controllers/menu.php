<?php
    $menu = [
        'entrees' => [
            'image' => '../images/carte-01.png',
            'choices' => [
                0 => [
                    'name' => 'Carpaccio de Bresaola',
                    'price' => 8,
                ],
                1 => [
                    'name' => 'Bruschetta Italienne',
                    'price' => 9,
                ],
                2 => [
                    'name' => 'Antipasto à l\'Italienne',
                    'price' => 10,
                ],
            ]
        ],
        'meals' => [
            'image' => '../images/carte-02.png',
            'choices' => [
                0 => [
                    'name' => 'Pizza Napolitaine',
                    'price' => 12,
                ],
                1 => [
                    'name' => 'Osso Bucco',
                    'price' => 14,
                ],
                2 => [
                    'name' => 'Escalope Milanaise',
                    'price' => 16,
                ],
            ]
        ],
        'deserts' => [
            'image' => '../images/carte-03.png',
            'choices' => [
                0 => [
                    'name' => 'Pana Cotta',
                    'price' => 6,
                ],
                1 => [
                    'name' => 'Tiramisu',
                    'price' => 8,
                ],
                2 => [
                    'name' => 'Zuccotto',
                    'price' => 10,
                ],
            ]
        ],
    ];
