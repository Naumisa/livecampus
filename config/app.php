<?php
    $appPath = "../app";
    $resourcesPath = "../resources";

    $controllerPath = "$appPath/controllers";
    $sectionPath = "$resourcesPath/sections";

    $page = filter_input(INPUT_GET, "page") !== null ? htmlspecialchars(filter_input(INPUT_GET, "page")) : 'home';

    $controller = file_exists("$controllerPath/$page.php") ? "$controllerPath/$page.php" : null;
    $vue = file_exists("$sectionPath/$page.php") ? "$sectionPath/$page.php" : "$sectionPath/home.php";

    $lang = require_once("$resourcesPath/lang/fr.php");
